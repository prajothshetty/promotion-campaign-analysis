#For succesfull implementaion of this particular project,it is advised to make a virtual environment and install the required dependencies which are being listed in the requirements file respectively.


*Objective:The main objective is to train a predictive model which allows the company to predict which customer's can  respond to an offer for a product or service and maximize the profit margins of the next marketing campaign.

*Context:The data is being collected after fews surveys done both online and offline for a retail company whose credentials are being masked off.There are several parameters which were taken into consideration for collection of data,they are as follows:

-AcceptedCmp1 - 1 if customer accepted the offer in the 1st campaign, 0 otherwise.
-AcceptedCmp2 - 1 if customer accepted the offer in the 2nd campaign, 0 otherwise.
-AcceptedCmp3 - 1 if customer accepted the offer in the 3rd campaign, 0 otherwise.
-AcceptedCmp4 - 1 if customer accepted the offer in the 4th campaign, 0 otherwise.
-AcceptedCmp5 - 1 if customer accepted the offer in the 5th campaign, 0 otherwise.
-Response (target) - 1 if customer accepted the offer in the last campaign, 0 otherwise.
-Complain - 1 if customer complained in the last 2 years.
-DtCustomer - date of customer’s enrolment with the company.
-Education - customer’s level of education.
-Marital - customer’s marital status.
-Kidhome - number of small children in customer’s household.
-Teenhome - number of teenagers in customer’s household.
-Income - customer’s yearly household income.
-MntFishProducts - amount spent on fish products in the last 2 years.
-MntMeatProducts - amount spent on meat products in the last 2 years.
-MntFruits - amount spent on fruits products in the last 2 years.
-MntSweetProducts - amount spent on sweet products in the last 2 years.
-MntWines - amount spent on wine products in the last 2 years.
-MntGoldProds - amount spent on gold products in the last 2 years.
-NumDealsPurchases - number of purchases made with discount.
-NumCatalogPurchases - number of purchases made using catalogue.
-NumStorePurchases - number of purchases made directly in stores.
-NumWebPurchases - number of purchases made through company’s web site.
-NumWebVisitsMonth - number of visits to company’s web site in the last month.
-Recency - number of days since the last purchase.

*Source of data: UCI machine learning repository with due acknowledgements to (O. Parr-Rud. Business Analytics Using SAS Enterprise Guide and SAS Enterprise Miner. SAS Institute, 2014O. Parr-Rud. Business Analytics Using SAS Enterprise Guide and SAS Enterprise Miner. SAS Institute, 2014)
 
